import pandas as pd
import numpy as np
import random
import tensorflow as tf
import math

from sklearn.model_selection import train_test_split
from keras.layers import Dropout
from keras.layers import BatchNormalization as bn
from sklearn.preprocessing import LabelEncoder
from sklearn.neighbors import KNeighborsClassifier
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import BatchNormalization
from sklearn.metrics import mean_squared_error
from sklearn import preprocessing
from sklearn.cluster import KMeans

class KmeansClassifier:
    
    # Constructor
    def __init__(self, data, n_clusters, init): 
        self.data = data
        self.n_clusters = n_clusters
        self.init = init
        
    
    def clustering(self):
        data_std = preprocessing.scale(self.data)
        data_std = pd.DataFrame(data_std)
        # Fitting K-Means to the dataset
        kmeans = KMeans(n_clusters = self.n_clusters, init = self.init, random_state = random.choice(range(0, 100)))
        y_kmeans = kmeans.fit_predict(data_std)

        #beginning of  the cluster numbering with 1 instead of 0
        y_kmeans1=y_kmeans
        y_kmeans1=y_kmeans+1

        # New Dataframe called cluster
        cluster = pd.DataFrame(y_kmeans1)

        # Adding cluster to the Dataset1
        self.data['cluster'] = cluster

        #Mean of clusters
        kmeans_mean_cluster = pd.DataFrame(round(self.data.groupby('cluster').mean(),1))
        centers = list()
        key = kmeans_mean_cluster[kmeans_mean_cluster.keys()[0]]
        for i in key.keys():
            c = list()
            center = list()
            for j in range(0, len(kmeans_mean_cluster.keys())):
                c.append(kmeans_mean_cluster[kmeans_mean_cluster.keys()[j]][i])
            center = [[i], c]
            centers.append(center)
        
        return centers

class Path:
    
    def __init__(self, path):
        self.path = path
    
    def predictGroup(self, centers):
        dist = list()
        for i in range(0, len(centers)):
            dist.append(mean_squared_error(self.path, centers[i][1]))
        return centers[np.argmin(dist)][0][0]

class BasicKnnClassifier: 

    # Constructor
    def __init__(self, data, inputs, outputs, train_size, classes, metric, k, loss=0): 
        self.train_size = train_size
        self.metric = metric
        self.inputs = inputs
        self.outputs = outputs
        self.classes = classes
        self.loss = loss
        self.k = k
        self.data = data
    
    # Split the dataset
    def split(self):
        # input dataset
        X = self.data[self.inputs]

        # output dataset
        y = self.data[[self.outputs]]

        # Spliting the dataset in two parts : training set (X_train, y_train) and testing set (X_test, y_test)  
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=self.train_size, random_state=random.choice(range(0, 100)))
        return X_train, X_test, y_train, y_test
    
    # Training function
    def train(self):
	    X_train, X_test, y_train, y_test = self.split()
	    losses = list()
        
        # This is the selection of the value of k which minimizes the loss, but it takes too much time
	    """for i in range(1, len(X_train)):
		# after analysis, 18 is the value of k that minimize the loss, for the k nearest neighbors' algorithm
		knn = KNeighborsClassifier(n_neighbors=i, metric=self.metric)
		knn.fit(X_train, y_train)

		# Predicted labels
		y_pred = knn.predict(X_test)

		# Computing the loss
		losses.append(mean_squared_error(y_pred, y_test))

	    self.loss= min(losses)
	    self.k = np.argmin(losses)+1"""

	    # after analysis, 18 is the value of k that minimize the loss, for the k nearest neighbors' algorithm
	    knn = KNeighborsClassifier(n_neighbors=self.k, metric=self.metric)
	    knn.fit(X_train, y_train)
	    # Predicted labels
	    y_pred = knn.predict(X_test)

	    # Computing the loss
	    self.loss = mean_squared_error(y_pred, y_test)

	    return knn

class DeepClassifier: 

    # Constructor
    def __init__(self, data, inputs, outputs, train_size, classes, epochs, batch_size): 
        self.train_size = train_size
        self.inputs = inputs
        self.outputs = outputs
        self.classes = classes
        self.data = data
        self.epochs = epochs
        self.batch_size = batch_size
    
    # Split the dataset
    def split(self):
        # input dataset
        X = self.data[self.inputs]

        # output dataset
        y = self.data[[self.outputs]]

        # Spliting the dataset in two parts : training set (X_train, y_train) and testing set (X_test, y_test)  
        X_train, X_test, y_train, y_test = train_test_split(X, y, train_size=self.train_size, random_state=random.choice(range(0, 100)))
        return X_train, X_test, y_train, y_test
    
    def model_network(self, metrics, loss, optimizer):
        model = Sequential()
        model.add(Dense(512, activation='relu', input_shape=(len(self.inputs),)))
        model.add(BatchNormalization())
        model.add(Dense(512, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dense(512, activation='relu'))
        model.add(BatchNormalization())
        model.add(Dense(2, activation='softmax'))

        model.compile(optimizer=optimizer,
                        loss=loss,
                        metrics=metrics)
        return model
    
    # Training function
    def train(self, model):
        X_train, X_test, y_train, y_test = self.split()

        # x_train and y_train are Numpy arrays --just like in the Scikit-Learn API.
        model.fit(X_train, y_train, epochs=self.epochs, batch_size=self.batch_size)
        
        return model
    
class Node:

    # Constructor
    def __init__(self, year , month, day, torUseV, os, ass, cc, label=0): 
        self.year = year
        self.month = month
        self.day = day
        self.torUseV = torUseV
        self.os = os
        self.ass = ass
        self.cc = cc
        
    # Prediction
    def predictLabel_with_knn(self, knn):
        Node = [[self.year, self.month, self.day, self.torUseV, self.os, self.ass, self.cc]]
        self.label = knn.predict(Node)[0]
        return 'announced' if (knn.predict(Node)[0] == 1) else 'not announced'
	
    def predictLabel_with_dl(self, model):
        node = np.array([np.array([self.year, self.month, self.day, self.torUseV, self.os, self.ass, self.cc])])
        self.label = np.argmax(model.predict(node)[0])
        return 'announced' if (np.argmax(model.predict(node)[0]) == 1) else 'not announced'

#Functions used in training

def build_model(shape, loss, optimizer, metrics):
    model = Sequential([
        Dense(1024, activation='relu', input_shape=[shape]),
        Dense(512, activation='relu'),
        Dense(256, activation='relu'),
        Dense(128, activation='relu'),
        Dense(64, activation='relu'),
        Dense(32, activation='relu'),
        Dense(16, activation='relu'),
        Dense(8, activation='relu'),
        Dense(4, activation='relu'),
        Dense(2, activation='relu'),
        Dense(1)
    ])

    model.compile(loss=loss,
                optimizer=optimizer,
                metrics=metrics)
    return model

def train(model, X_train, y_train, epochs, validation_split, verbose):
    return model.fit(X_train, y_train, epochs=epochs, validation_split = validation_split, verbose=verbose)

def printHistory(history):
    hist = pd.DataFrame(history.history)
    hist['epoch'] = history.epoch 
    return print(hist)

def predict(model, x_test):
    return model.predict(x_test)

def clean_by_regression(paths, feature, inputs, train_size, epochs, printHist, name_of_saved_file, loss, optimizer, metrics):
    full = paths[paths[feature].notna()][[feature]+inputs]

    x = full[inputs]
    y = full[[feature]]

    # We split the data set 80% of training set and 20% of testing set
    X_train, X_test, y_train, y_test = train_test_split(x, y, train_size=train_size, random_state=random.choice(range(0, 100)))

    model = build_model(len(X_train.keys()), loss, optimizer, metrics)

    # Training with the model
    history = train(model, X_train, y_train, epochs, 0.2, 0)

    # Printing history
    if(printHist):    
        printHistory(history) 
    
    # Predict in download's columns of paths values not set
    for i in range(0, len(paths)):
        lgn = list()
        for j in range(0, len(inputs)):
            lgn.append(paths[inputs[j]][i])
        if(np.isnan(paths[feature][i])):
            ytest = np.array([np.array(lgn)])
            paths[feature][i] = predict(model, ytest)[0][0]
    # change the paths.csv file
    paths.to_csv(name_of_saved_file+'.csv', index=False)
    print("well done ! : {}".format(feature))


